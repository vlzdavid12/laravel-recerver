<div id="editUserModal" tabindex="-1" aria-hidden="true"
     class="fixed top-0 left-0 right-0 z-50 items-center justify-center hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full">
    <div class="relative w-full h-full max-w-2xl md:h-auto">
        <!-- Modal content -->
        <div class="relative bg-white rounded-lg shadow dark:bg-gray-700 p-4">
            <!-- Head -->
            <div class="flex items-center">
                <div class="flex-none w-14">
                    <img class="w-10 h-10 rounded-full block mx-auto"
                         src="https://flowbite.com/docs/images/people/profile-picture-1.jpg"
                         alt="Jese image">
                </div>
                <div class="flex-initial w-64">
                    <div class="pl-3">
                        <div class="text-base font-semibold">Neil Sims</div>
                        <div class="font-normal text-gray-500">neil.sims@flowbite.com</div>
                    </div>
                </div>
            </div>
            <!-- End Head -->
            <p class="text-justify text-gray-700 py-6 text-sm">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
                scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
                into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                software like Aldus PageMaker including versions of Lorem Ipsum.
            </p>

            <div class="flex items-end gap-4">
                <div class="block">
                    <label for="countries" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Dias</label>
                    <select id="countries"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option selected>Seleccione</option>
                        <option value="US">1 Dia</option>
                        <option value="CA">2 Dias</option>
                        <option value="FR">3 Semanas</option>
                        <option value="DE">1 Mes</option>
                    </select>
                </div>
                <button type="button"
                        class="focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2  dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">
                    Reservar
                </button>
            </div>


        </div>
    </div>
</div>
