## Booking with Laravel and Docker

### Configuration or Install

<b>Note:</b> Before starting it is important to change env.example to .env

First step download project and go to folder
```
cd laravel-recerver
```

Seconds step run laravel

Link Downloader: <a href="https://getcomposer.org/download/"> Composer</a>

```
composer i
sail up
/// Shup Down Server off
sail down
```

<b>Note:</b> If there is an error in the 'sail up' command

Solutions:
```
composer require laravel/sail --dev
php artisan sail:install
```

More information: <a href="https://laravel.com/docs/9.x/sail">Laravel Sail</a>

Third step open another console and write
```
sail npm i
sail npm run dev
```

### Url Laravel and Email

http://localhost -> Laravel <br/>
http://127.0.0.1:8025/ -> Email Service


## Contributions
https://flowbite.com/docs/ <br/>
https://laravel.com/ <br/>
https://tailwindcss.com/ <br/>
https://nodejs.org/en/ <br/>
